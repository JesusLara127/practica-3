package Controlador;

import Modelo.Gasolina;
import Modelo.Bomba;
import Vista.dlgBomba;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    private Bomba bom;
    private dlgBomba vista;
    private int contador;
    
    public Controlador(Bomba bom, dlgBomba vista){
        this.bom=bom;
        this.vista=vista;
        
        this.vista.btnIniciarBomba.addActionListener(this);
        this.vista.btnRegistrar.addActionListener(this);
        
    }
    public void iniciarVista(){
        this.vista.setTitle("BOMBA");
        this.vista.setSize(781, 408);
        this.vista.setVisible(true);
    }

    public static void main(String[] args) {
        Bomba obj=new Bomba();
        dlgBomba vista=new dlgBomba(new JFrame(),true);
        Controlador controlador=new Controlador(obj,vista);
        controlador.iniciarVista();
    }
    
    private Boolean isVacio(){
        return this.vista.txtNumeroBomba.getText().equals("")||
               this.vista.txtPrecio.getText().equals("");
    }
    private int isValidoIniciarBomba(){
        try{
            if(Float.parseFloat(this.vista.txtNumeroBomba.getText()) <= 0||
                Float.parseFloat(this.vista.txtPrecio.getText()) <= 0){
                return 1;
                }
            
        }
        catch(NumberFormatException ex){
            
            return 2;
        }
        return 0;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.vista.btnIniciarBomba){
            if(this.isVacio()){
                JOptionPane.showConfirmDialog(this.vista,
                        "Tienes campos vacios","BOMBA",JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(this.isValidoIniciarBomba()==1){
                JOptionPane.showMessageDialog(this.vista, "Pon numeros validos","BOMBA",JOptionPane.ERROR_MESSAGE);
                return;
            }
            else if(this.isValidoIniciarBomba()==2){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio un error","BOMBA",JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.bom.iniciarBomba(Integer.parseInt(this.vista.txtNumeroBomba.getText()),
                new Gasolina(this.vista.cmbTipo.getSelectedIndex()+1,this.vista.cmbTipo.getSelectedItem().toString(),
                    Float.parseFloat(this.vista.txtPrecio.getText())),
                        this.vista.jSCantidad.getValue(),0.0f);
            this.vista.btnIniciarBomba.setEnabled(false);
            this.vista.txtNumeroBomba.setEnabled(false);
            this.vista.txtPrecio.setEnabled(false);
            this.vista.jSCantidad.setEnabled(false);
            this.vista.cmbTipo.setEnabled(false);
            this.vista.txtCantidad.setEnabled(true);
            this.vista.btnRegistrar.setEnabled(true);
        }
        if(e.getSource()==this.vista.btnRegistrar){
            if(this.vista.txtCantidad.getText().equals("")){
                JOptionPane.showConfirmDialog(this.vista,
                        "Tienes campos vacios","BOMBA",JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(this.isValidoRegistrar()==1){
                JOptionPane.showMessageDialog(this.vista, "Pon numeros validos","BOMBA",JOptionPane.ERROR_MESSAGE);
                return;
            }
            else if(this.isValidoRegistrar()==2){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio un erroR","BOMBA",JOptionPane.ERROR_MESSAGE);
                return;
            }
            float venta=this.bom.venderGasolina(Float.parseFloat(this.vista.txtCantidad.getText()));
            if(venta!=0.0f){
                this.vista.lblCosto.setText(String.valueOf(venta));
                this.vista.lblTotalVenta.setText(String.valueOf(this.bom.ventasTotales()));
                this.contador++;
                this.vista.txtContador.setText(String.valueOf(this.contador));
                this.vista.jSCantidad.setValue((int)this.bom.inventarioGasolina());
            }
            }
    }
    private int isValidoRegistrar(){
        try{
            if(
                Float.parseFloat(this.vista.txtCantidad.getText()) <= 0){
                return 1;
                }
            
        }
        catch(NumberFormatException ex){
            
            return 2;
        }
        return 0;
    }
}




























