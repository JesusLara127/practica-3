/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author PC
 */
public class Gasolina {
    private int IDGasolina;
    private String tipo;
    private float precio;
    
    public Gasolina(){
        this.IDGasolina=0;
        this.precio=0.0f;
        this.tipo="";  
    }
    public Gasolina(int IDGasolina, String tipo, float precio) {
        this.IDGasolina = IDGasolina;
        this.tipo = tipo;
        this.precio = precio;
    }
    public Gasolina(Gasolina obj){
        this.IDGasolina= obj.IDGasolina;
        this.precio=obj.precio;
        this.tipo=obj.tipo;
    }

    public int getIDGasolina() {
        return IDGasolina;
    }

    public void setIDGasolina(int IDGasolina) {
        this.IDGasolina = IDGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
}
